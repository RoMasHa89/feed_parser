<?php
/**
 * @file
 * BlogFeed Class.
 */

/**
 * BlogFeed Class Doc Comment.
 *
 * Class with methods, that provides parsing XML.
 *
 * @category Class
 *
 * @package FeedParser
 *
 * @author Alex Romantsov
 *
 * @license http://www.gnu.org/copyleft/gpl.html GNU General Public License.
 *
 * @link http://
 */
class BlogFeed
{
    var $posts = array();

    function __construct($file_or_url) 
    {
        $file_or_url = $this->resolveFile($file_or_url);
        if (!($x = simplexml_load_file($file_or_url))) {
            return;
        }

        foreach ($x->channel->item as $item) {
            $post = new stdClass();
            $post->date = (string) $item->pubDate;
            $post->ts = strtotime($item->pubDate);
            $post->link = (string) $item->link;
            $post->title = (string) $item->title;
            $post->text = (string) $item->description;

            // Get XML namespaces.
            $namespaces = $x->getNamespaces(true);
            foreach ($namespaces as $key => $value) {
                if ($key == 'media') {
                    $tmp = $item->children($key, true);
                    if ($tmp->content && $tmp->content[0]->attributes()) {
                        $attrs = $tmp->content[0]->attributes();
                        $post->image = strval($attrs['url']);
                    }
                }
                elseif ($key == 'content') {
                    $post->image = (string) $item->enclosure['url'];
                }
            }

            // Create summary as a shortened body and remove images,
            // extraneous line breaks, etc.
            $post->summary = $this->summarizeText($post->text);

            $this->posts[] = $post;
        }
    }

    private function resolveFile($file_or_url) 
    {
        if (!preg_match('|^https?:|', $file_or_url)) {
            $feed_uri = $_SERVER['DOCUMENT_ROOT'] . '/shared/xml/' . $file_or_url;
        }
        else {
            $feed_uri = $file_or_url;
        }

        return $feed_uri;
    }

    /**
   * Truncate summary line to 100 characters.
   *
   * @param string $summary
   *   Text.
   *
   * @return string
   *   Shorten text.
   */
    private function summarizeText($summary) 
    {
        $summary = strip_tags($summary);
        $max_len = 100;
        if (strlen($summary) > $max_len) {
            $summary = substr($summary, 0, $max_len) . '...';
        }

        return $summary;
    }

}
