(function ($) {
    Drupal.behaviors.feed_parser = {
        attach: function (context, settings) {
            $('input.form-spinner', context).spinner({min: 1, max: 100});
        }
    };
})(jQuery);

