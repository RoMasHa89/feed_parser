(function ($) {
    Drupal.behaviors.feed_parser = {
        attach: function (context, settings) {
            $('body').once(
                function () { // Make this function execute once on body element. Else we could have problems with multiple timer start
                    var ajax = new Drupal.ajax(false, false, {url: '/ajax_forms/ajax/nojs'});
                    var timer = window.setInterval(
                        function () { // Creating timer for request send
                            $(document).queue(
                                function () { // Adding code to queue
                                    //console.log(Drupal.settings.feed_parser.progress);
                                    ajax.eventResponse(ajax, {}); //Request and execute
                                    $(document).dequeue();
                                }
                            );
                            return false;
                        }, 2000
                    ); //Set timer interval
                }
            )
        }
    }
})(jQuery);